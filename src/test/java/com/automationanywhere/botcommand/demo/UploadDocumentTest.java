package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Map;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class UploadDocumentTest {

    UploadDocument command = new UploadDocument();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{

                {"https://txyaypg3p6.execute-api.us-west-2.amazonaws.com/dev/v8/requestUploadUrl","C:/iqbot/Document Samples/Stock Documents/Bank Statements/0146_Purchase_0146-08142019-162639_1.png",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String URL, String TheFile, String TheScore){
        StringValue d = command.action(URL,TheFile);
       System.out.println(d.toString());

    }
}
