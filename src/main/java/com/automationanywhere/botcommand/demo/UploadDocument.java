package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import static com.automationanywhere.commandsdk.model.DataType.STRING;


/**
 * @author Bren Sapience
 *
 *
 */

@BotCommand
@CommandPkg(label="Upload Document", name="Upload Document", description="Upload Document to v8 Engine", icon="pkg.svg",
        node_label="Upload Document",
        return_type= STRING, return_label="Assign the output to variable", return_required=true)

public class UploadDocument {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Execute
    public StringValue action(
            //https://txyaypg3p6.execute-api.us-west-2.amazonaws.com/dev/v8/requestUploadUrl
            @Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = "URL to v8 Engine Service", default_value_type = STRING) @NotEmpty String V8URL,
            @Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Input File Path", default_value_type = STRING) @NotEmpty String TheFile
    )  {
        String RES_URL_BASE = "https://txyaypg3p6.execute-api.us-west-2.amazonaws.com/dev/v8/";
        String JSONFILERESPONSE = "";
        if ("".equals(V8URL)) {
            throw new BotCommandException(MESSAGES.getString("emptyInputString", "V8URL"));
        }
        if ("".equals(TheFile)) {
            throw new BotCommandException(MESSAGES.getString("emptyInputString", "InputFilePath"));
        }

        try {
            Unirest.setTimeouts(0, 0);
            String JsonBody = "{\"name\":\""+TheFile+"\",\"type\":\"image/png\"}";
            System.out.println("DEBUG - JSON  Body: "+JsonBody);
            HttpResponse<String> response = Unirest.post(V8URL)
                    .header("Content-Type", "application/json")
                    .body(JsonBody)
                    .asString();

            System.out.println("DEBUG - Raw Res: "+response.getBody());
            JSONParser parse = new JSONParser();
            JSONObject jobj = (JSONObject)parse.parse(response.getBody());

            System.out.println("DEBUG - JSON Raw Result: "+jobj);
            String FileID =jobj.get("fileId").toString();
            String UploadURL = jobj.get("uploadURL").toString();

            String FileDetailsURL = RES_URL_BASE + FileID;

            //System.out.println(response.getBody());
            System.out.println(FileID);
            System.out.println(UploadURL);

            InputStream file = new FileInputStream(new File(TheFile));
            URL url = new URL(UploadURL);

            File myFile = new File(TheFile);
            FileEntity fileEntity = new FileEntity(myFile);

            CloseableHttpClient httpClient = HttpClients.createSystem();
            HttpPut httpPut = new HttpPut(UploadURL);
            //httpPut.setHeader("Content-Type", "image/png");
            httpPut.setEntity(fileEntity);
            try {
                CloseableHttpResponse response0 = httpClient.execute(httpPut);
                int RetCode = response0.getStatusLine().getStatusCode();
                if(RetCode!=200){
                    // Error
                }else{
                    HttpResponse<String> response1 = CheckFileUploaded(FileDetailsURL);
                    JSONFILERESPONSE = response1.getBody();
                    //System.out.println("DEBUG F:"+response1.getBody());


                }

            } catch (IOException | InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        }catch(UnirestException e){
            // TBD
        }catch(ParseException e){

        } catch (IOException e) {
            e.printStackTrace();
        }
        return new StringValue(JSONFILERESPONSE);
    }
    private HttpResponse<String> CheckFileUploaded(String CheckURL) throws UnirestException, ParseException, InterruptedException {
        Unirest.setTimeouts(0, 0);
        Boolean ProcessDone = false;
        HttpResponse<String> response1 = null;
        while(!ProcessDone){
            response1 = Unirest.get(CheckURL).asString();
            JSONParser parse0 = new JSONParser();
            JSONObject jobj0 = (JSONObject)parse0.parse(response1.getBody());
            if(!jobj0.containsKey("message")){
                ProcessDone = true;
            }
            //System.out.println("Sleeping..");
            Thread.sleep(3000);
        }
        return response1;


    }

}